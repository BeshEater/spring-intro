package com.besheater.training.spring.intro.beans;

public class BeanA implements InfoBean {

    private InfoBean firstBean;
    private InfoBean secondBean;

    public BeanA (InfoBean firstBean, InfoBean secondBean) {
        this.firstBean = firstBean;
        this.secondBean = secondBean;
    }

    @Override
    public void printInfo(InfoBean parent) {
        InfoBean.super.printInfo(null);
        firstBean.printInfo(this);
        secondBean.printInfo(this);
    }
}