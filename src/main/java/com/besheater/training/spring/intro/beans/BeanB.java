package com.besheater.training.spring.intro.beans;

import com.besheater.training.spring.intro.Message;

import java.util.List;

public class BeanB implements InfoBean {

    private List<Message> messages;

    public BeanB() { }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void printMessages() {
        messages.forEach(System.out::println);
    }
}