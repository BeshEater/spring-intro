package com.besheater.training.spring.intro.beans;

public interface InfoBean {

    default void printInfo(InfoBean parent) {
        String thisClassName = getClass().getSimpleName();
        String parentClassName = parent == null ? "" : parent.getClass().getSimpleName();
        System.out.printf("Class = %s | ParentClass = %s\n", thisClassName, parentClassName);
    }
}