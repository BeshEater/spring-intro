package com.besheater.training.spring.intro.beans;

import java.util.List;

public class BeanC implements InfoBean {

    private List<InfoBean> infoBeans;

    public BeanC (List<InfoBean> infoBeans) {
        this.infoBeans = infoBeans;
    }

    @Override
    public void printInfo(InfoBean parent) {
        InfoBean.super.printInfo(parent);
        infoBeans.forEach(bean -> bean.printInfo(this));
    }
}