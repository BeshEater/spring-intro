package com.besheater.training.spring.intro;

import com.besheater.training.spring.intro.beans.BeanA;
import com.besheater.training.spring.intro.beans.BeanB;
import com.besheater.training.spring.intro.beans.BeanD;
import com.besheater.training.spring.intro.beans.BeanE;

import java.util.Arrays;
import java.util.List;

public class BeanDriver {

    public static void main( String[] args ) {
        BeanA beanA = new BeanA(new BeanD(), new BeanE());
        beanA.printInfo(null);
        BeanB beanB = new BeanB();
        List<Message> messages = Arrays.asList(
                new Message("Msg #1"), new Message("Msg #2"), new Message("Msg #3"));
        beanB.setMessages(messages);
        beanB.printMessages();
    }
}